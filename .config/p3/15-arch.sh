#!/usr/bin/env sh

# Where packages are built
var "BUILDDIR" "/tmp/makepkg"

# Where to clone package build files
var "AURDEST" "/var/cache/aur"
