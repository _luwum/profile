#!/usr/bin/env sh

var "PATH" "$PATH:${XBH}/bin"
var "LC_ALL" "en_US.UTF-8"
var "LANG" "en_US.UTF-8"
var "TZ" "America/Chicago"
var "TERM" "xterm-256color"
var "TMPDIR" "/tmp"
var "PAGER" "/usr/bin/bat"
var "EDITOR" "/usr/bin/nvim"
